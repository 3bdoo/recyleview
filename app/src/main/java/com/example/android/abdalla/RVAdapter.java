package com.example.android.abdalla;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.PersonViewHolder> {


    List<Person> persons; // The person list

    // Our adapter constructor
    RVAdapter(List<Person> persons){
        this.persons = persons; // Setting tha passed list to parsons list
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Getting the item view and return the ViewHolder
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list, parent, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PersonViewHolder holder, int position) {
        // Loop into the list and apply each item to the ViewHolder
        holder.FruitName.setText(persons.get(position).name);
        holder.FruitPrice.setText(persons.get(position).price);
        holder.FruitImage.setImageResource(persons.get(position).image);
    }

    @Override
    public int getItemCount() {
        return persons.size();
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


    /**
     * This is the ViewHolder.
     * View Holder is a sub-class of the adapter,
     * and used to describe the elements in one item of the RecyclerView
     */
    public static class PersonViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView FruitName;
        TextView FruitPrice;
        ImageView FruitImage;

        PersonViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);
            FruitName = (TextView)itemView.findViewById(R.id.Fruit_Name);
            FruitPrice = (TextView)itemView.findViewById(R.id.Fruit_Price);
            FruitImage = (ImageView)itemView.findViewById(R.id.Fruit_Image);
        }

    }

}
