package com.example.android.abdalla;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    // Making a list of type Person, and naming it persons
    private List<Person> persons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Getting the RecyclerView from our activity
        RecyclerView rv = (RecyclerView) findViewById(R.id.rv);

        rv.setHasFixedSize(true); //since our list has a fixed element size, we use this method to improve performance

        //we call this method to add our data into the list
        initializeData();

        LinearLayoutManager llm = new LinearLayoutManager(this); //adding the LinearLayoutManager and passing the context
        rv.setLayoutManager(llm); // setting the LinearLayoutManager to our RecyclerView
        RVAdapter adapter = new RVAdapter(persons); //Making new object of our Adapter and send the list to it
        rv.setAdapter(adapter); // setting the Adapter to our RecyclerView

    }


    /**
     * This method is to add data to the persons list
     */
    private void initializeData(){
        persons = new ArrayList<>();
        persons.add(new Person("Banana", "Price = 10 L.E", R.drawable.images));
        persons.add(new Person("Apple", "Price = 15 L.E", R.drawable.apple));
        persons.add(new Person("Mango", "Price = 12 L.E", R.drawable.mango));
        persons.add(new Person("WaterMelon", "Price = 20 L.E", R.drawable.watermelon));
        persons.add(new Person("Orange", "Price = 13 L.E", R.drawable.orange));
        persons.add(new Person("Strawberry", "Price = 8 L.E", R.drawable.strawberry));
        persons.add(new Person("Grapes", "Price = 12 L.E", R.drawable.grapes));
        persons.add(new Person("PineApple", "Price = 20 L.E", R.drawable.pineapple));
        persons.add(new Person("Peach", "Price = 16 L.E", R.drawable.peach));
        persons.add(new Person("Cherry", "Price = 24 L.E", R.drawable.cherry));
        persons.add(new Person("For Orders", "Pls Call +64 508 673 376", R.drawable.phone));
    }
}
